# Pizza Dough

Makes: 3 pizza bases

Adapted from: [https://www.taste.com.au/recipes/pizza-dough/61da0d5a-abe7-4a62-a805-c9c4a729d718](https://www.taste.com.au/recipes/pizza-dough/61da0d5a-abe7-4a62-a805-c9c4a729d718)

![Pizza Dough.webp](Pizza Dough.webp)

## Ingredients

- 375mL (1 1/2 cups) warm water
- Pinch of caster sugar
- 2 tsp (7g/1 sachet) dried yeast
- 600g (4 cups) plain flour, plus extra for dusting
- 1 tsp salt
- 60mL (1/4 cup) olive oil, plus extra for brushing

## Method
1. Combine water, yeast and sugar in a small bowl. Set aside for 5 mins or until foamy.
2. Combine flour and salt in a large bowl and make a well in the centre. Add the yeast mixture and oil. Mix until combined, then use hands to bring dough together in the bowl.
3. Turn the dough out onto a lightly floured surface and knead for 10 mins or until smooth and elastic.
4. Brush an even larger bowl with oil. Place the dough into this bowl and coat the top of the dough with oil. Cover with plastic wrap and set asided in a warm, draught-free place to rise for 30mins or until the dough roughly doubles in size.
5. Remove dough from bowl onto a lightly floured surface. Knead dough into a long cylinder and divide into thirds. Use a rolling pin with an outwards from centre motion to roll dough into about a 20cm disk
6. Add your favourite toppings and bake at 220C (430F) watching for the dough to crisp up (approx 5-10mins)
