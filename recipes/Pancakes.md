# Pancakes - Easy Mode

![Pancakes](Pancakes.jpg){width=378 height=504}

## Ingredients
- 1 large egg
- 1 cup (20mL) milk
- 1 cup self raising flour

## Method
1. Whisk eggs and milk together. Add sifted flour and beat until batter is smooth and lump free. Add in any options you like here (mashed banana, berries, etc)
2. Heat a non-stick pan over low-medium heat. Brush a small amount of oil.
3. Use a ladle to pour batter into pan. Cook until bubbles appear and flip over.

## Tips
- if using plain flour, add 2 teaspoons of baking powder
- add 2 teaspoons of baking powder or extra fluffiness
- try 1 teaspoon of vanilla extract for yumminess
