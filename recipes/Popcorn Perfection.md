# Popcorn Perfection

Make perfect popcorn without burning it!

## Ingredients

- 2 tablespoons of oil
- 1/3 cup of popping corn kernels
- salt and butter to taste

## Method

1. Heat oil in a thick base pan or saucepan on medium high heat with 3-4 of the popping corn kernels. Place lid on pan and wait for these to pop.
2. When these kernels have popped, carefully remove them into a bowl.
3. Add the rest of the kernels in an even single layer. Cover, then REMOVE FROM HEAT and count 30 seconds. Doing this allows oil to heat to the right temperature then brings the rest of the kernels to an even, near-popping temperature. When they are put back on the heat they should pop at about the same time.
4. Return the pan to the heat. Cover, but if possible, leave the lid slightly ajar to allow steam to escape, which makes for crispier popcorn.
5. Gently agitate the corn as it's popping.
6. Once the popping slows to several seconds between pops, remove pan from heat and tip popcorn into a bowl.
7. Turn off heat. The residual heat in the pan should be able to melt the butter. Once it is melted you can return the popcorn to the pan, with salt to taste and shake it up with the lid on to coat the popcorn.
8. Tip back into bowl and enjoy!
