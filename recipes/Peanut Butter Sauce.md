# Peanut Butter Sauce (for Vietnamese fresh spring rolls)

![Peanut Butter Sauce](Peanut Butter Sauce.jpg){width=378 height=504}

## Ingredients
- 1/2 cup of Hoi Sin Sauce
- 3 tablespoons of Peanut Butter
- 3/4 cup of water
- 2 shallots (spring onions) minced
- 1 teaspoon of oil
- 2 tablespoons of crushed roasted peanuts (optional)

## Method
1. Turn on the stove to low heat.
2. Put oil and minced shallots into a small pot on the stove. Fry until fragant.
3. Poor in roughly half the Hoi Sin Sauce and water and mix with a wooden spoon.
4. Add the Peanut Butter a bit at a time and stir until the Peanut Butter is incorporated.
5. Add the rest of the Hoi Sin Sauce and water. Mix until smooth and heated through.
6. Optionally add extra crushed peanuts if using smooth Peanut Butter above.
