# Eggplant with Garlic Sauce 

![Eggplant with Garlic Sauce](Eggplant with Garlic Sauce.png){width=640 height=659}

Adapted from this recipe by [Made With Lau](https://www.youtube.com/watch?v=lWJpa0MRHAs)

- Prep time: 20 minutes
- Cook time: 20 minutes
- Serves: 4

## INGREDIENTS
- 500g or 1 large Eggplant
- Half a Red Capsicum (Red Bell Pepper) diced
- 2 Shallot Stalks (Scallion) or 1/4 Onion finely chopped
- 4 cloves of Garlic finely chopped
- Thumbnail sized knob of Ginger finely chopped
- 2 Birds Eye Chillies finely chopped (optional)
- 150g of Mince Pork or Beef (omit for vegetarian option)
- Light Soy Sauce
- Dark Soy Sauce
- Oyster Sauce
- Vinegar or Lemon Juice
- Sugar (Brown)
- Cornstarch
- Sesame Oil
- Vegetable Oil
- Black Pepper
- Hot Water
- Shallot (Scallion) green tops chopped for garnish

[Other vegetables such as 2 sticks of diced Celery and/or 1 diced Carrot can be added as an option]

## METHOD

### Marinate the meat
1. To the minced meat, add <strong>2 tablespoons of Light Soy Sauce, 4-5 turns of ground Black Pepper, 1 teaspoon of Sesame Oil</strong> in a bowl.
2. Mix and leave to marinade for about 10 mins while you prepare the rest of the ingredients.

### Prepare the Eggplant
1. Boil some water in a steaming pot.
2. Cut the Eggplant into about 1 inch (2.5cm) cubes.
3. Place the Eggplant skin side down onto a steaming tray. This allows the Eggplant to cook more evenly as the skin side will take more heat. If you have inside flesh pieces of the Eggplant that do not have any skin, leave them till last to put on top in the steaming tray).
4. Steam the Eggplant for about 15mins or until the flesh is soft but not too mushy.

<strong>Alternatively</strong> you can microwave the Eggplant until soft. This will depend on the power of your microwave.

### Prepare the Sauce
1. In a bowl, mix together:

	- 2 tablespoons of Light Soy Sauce
	- 1 tablespoon of Dark Soy Sauce
	- 1 tablespoon of Oyster Sauce
	- 1 tablespoon of Vinegar or Lemon Juice
    - 1 teaspoon of Sugar (brown)
	- 1 teaspoon of Cornstarch
	- 2 tablespoons of Hot Water
	
2. Set this aside until the frying stage.

### Prepare the Vegetables
1. Dice the Capsicum (Red Bell Pepper).
2. Chop the Shallot Stalks (Scallion) or Onion.
3. Finely chop the Garlic, Ginger and Chillis (seeds removed if desired).

At this stage, you could consider other vegetables you might have lying around. Some examples might include diced celery or carrot.

### Putting It All Together
1. Heat your wok to smoking, and add vegetable oil.
2. Fry the <strong>mince meat</strong> quickly. Toss until just browning and remove from heat back into a bowl.
3. Put the wok back onto the heat, add more vegetable oil and fry the <strong>chopped Garlic, Ginger, Chillies, Shallot Stalks/Onions</strong> for about 30 seconds, or until fragrant.
4. Add the <strong>remainder of the chopped vegetables (Capsicum, Celery, Carrots, etc.)</strong> to the wok and toss until they start to soften. Leave out the Eggplant until later below.
5. Now add the <strong>Sauce</strong> you made earlier to the wok and bring it to the boil.
6. Add the <strong>steamed Eggplant</strong> to the wok.
7. Add the <strong>fried minced meat</strong> back into the wok.
8. Stir through all the ingredients until coated with sauce.
9. Serve and garnish with chopped green shallot tops.
10. Enjoy with steamed rice!

