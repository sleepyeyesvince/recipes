# Easy banana cake in frying pan | 15 minutes Snacks recipe
[https://www.youtube.com/watch?v=XGlDZ7x_77c](https://www.youtube.com/watch?v=XGlDZ7x_77c)

![Easy banana cake in frying pan](Easy banana cake in frying pan.jpg){width=504 height=378}

## Ingredients

- 1 ripe banana

- 1 egg

- 1 pinch of salt

- 1/4 cup 50g sugar

- 2 tbsp oil

- 1/2 tsp (2.5mL) vanilla extract

- 1/2 cup (60g) all purpose flour

- 1/2 tsp (2g) baking powder

## Directions

1. Place banana and crack egg into a mixing bowl

2. Mash the banana as smooth as possible. Mix well.

3. Add a pinch of salt, 1/4 cup sugar, 2 tbsp oil, 1/2 tsp vanilla extract.

4. Sift 1/2 cup flour and 1/2 tsp baking powder into mixture.

5. Brush some oil in pan.

6. Pour mixture in pan.

7. Use very low flame. Bake it for 4-5 minutes. (Do not use medium or high flame. In that case, it may burn.)

8. Flip it over. Bake this side for 1-2 minutes
