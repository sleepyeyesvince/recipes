# Chickpea and Miso Pasta
#Vegan #Protein #Carbs #Cheap #Kids #Fusion

![Chickpea and Miso Pasta](Chickpea and Miso Pasta.jpg){width=504 height=378}

Adapted from this recipe by [Hetty McKinnon](https://www.abc.net.au/news/2021-09-09/chickpea-and-miso-pasta/100437450)

- Prep time: 15 minutes
- Cook time: 15 minutes
- Serves: 4

## INGREDIENTS
- 1 packet (400g) of spaghetti
- 2 cans (500g) of chickpeas, drained
- 4-5 cloves of garlic, chopped
- 2 teaspoons red chilli flakes (I substituted with smoked paprika)
- 4 tablespoons (1/4 cup) white miso paste
- 1 teaspoon ground coriander seeds
- 2 teaspoons sesame oil
- Extra virgin olive oil
- 2 teaspoons black pepper
- Basil leaves, diced
- Half a lemon, juiced

[Other vegetables such as 2 sticks of diced Celery and/or 1/2 diced Onion can be added as an option to add more bulk]

## METHOD

1. Boil a large pot of water for pasta and cook according to instructions, minus 1 minute. When done, reserve about 2 cups of pasta water into a bowl. Drain pasta
2. While pasta is cooking, put drained chickpeas, chilli flakes (or smoked paprika), ground coriander seeds, sesame oil, and 1 tablespoon of olive oil into a medium bowl and mash with a fork or potato masher until it is a chunky, thick paste. Alternatively, use a food processor until chunky, but not pureed.
3. Heat a large frying pain on medium-high heat. Add 3 tablespoons of olive oil. Add garlic, pepper and diced vegetables and stir until vegetables softened.
4. Add the chickpea mixture into the frying pan. Combine and stir for about 3-4 minutes until the chickpea mixture starts to stick to the pan.
5. Add 1 cup of the reserved pasta water to the chickpeas in the pan and stir to combine.
6. Add the pasta to the pan and toss with cooking tongs to coat. Gradually add the remaining pasta water to the pan, about 1/4 cup at a time and continue tossing pasta through mixture until it is saucy, but sticks to the pasta.
7. Turn off the heat, add the lemon juice and give one last toss through.
8. Drizzle some more extra virgin olive oil over the pasta and top with diced basil leaves.
8. Serve with cracked pepper to taste.
