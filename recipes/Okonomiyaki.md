# Okonomiyaki

![Okonomiyaki](Okonomiyaki.jpg){width=504 height=378}

Adapted from:

[https://www.laurainthekitchen.com/recipes/cabbage-fritters/](https://www.laurainthekitchen.com/recipes/cabbage-fritters/)

## INGREDIENTS

- 4 cups of shredded cabbage
- 1/2 cup of chopped shallots
- 2 eggs
- 1/3 cup of milk
- 1/2 cup of all purpose flour
- 1/2 teaspoon of baking powder
- 1/2 teaspoon of salt, to taste
- 1/4 teaspoon of black pepper, to taste
- 1 teaspoon of sesame oil
- 1 tablespoon of soy sauce
- Other spices: I recommend garlic powder, corriander powder, smoked paprika or experiment with your favourite mixture of spices
- mixed dried herbs: this is optional, but this dish is so versatile you can add whatever is in your pantry

## METHOD

1. In a mixing bowl, combine the dry ingredients of the flour, baking powder, salt, black pepper, spices, and dried herbs.
2. To the dry ingredients, add the milk, eggs, sesame oil, and soy sauce. Whisk until smooth. Allow the batter to rest while you prepare the other ingredients
3. Shred the cabbage or slice it finely. Ensure the cabbage is dry before using or use a salad spinner to dry it after shredding.
4. Finely chop the shallots.
5. Add the cabbage and shallots to the batter and mix well
6. In a non stick pan, fry about 2 tablespoons of the batter per serve in a good amount of oil for crispiness.
7. Serve with barbecue sauce and Kewpie Mayonnaise.
8. If you have bonito flakes and/or toasted sesame seeds you can sprinkle them on top.
