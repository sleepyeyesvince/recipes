# recipes

My recipes :)

Please feel free to submit merge requests for any you would like to contribute.

- [Banana Bread](recipes/Banana Bread.md)
- [Chickpea and Miso Pasta](recipes/Chickpea and Miso Pasta.md)
- [Easy banana cake in frying pan](recipes/Easy banana cake in frying pan.md)
- [Eggplant with Garlic Sauce](recipes/Eggplant with Garlic Sauce.md)
- [Okonomiyaki](recipes/Okonomiyaki.md)
- [Pancakes - Easy Mode](recipes/Pancakes.md)
- [Peanut Butter Sauce (for Vietnamese fresh spring rolls)](recipes/Peanut Butter Sauce.md)
- [Pizza Dough](recipes/Pizza Dough.md)
- [Popcorn Perfection](recipes/Popcorn Perfection.md)
